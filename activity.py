# The goal of the activity is to be able to create a simple application that applies the four tenets of Object-Oriented Programming in the context of Python.

# Create an abstract class called Animal that has the following abstract methods:
# eat(food)
# make_sound()

# Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
# Properties:
#    Name
#    Breed
#   Age
# Methods:
#   Getters
#   Setters
#    Implementation of abstract methods
#   call()

from abc import ABC, abstractmethod

class Animal(ABC):
    def __init__(self, name, breed, age):
        self.name = name;
        self.breed = breed;
        self.age = age;
        super().__init__();

    # @abstractmethod
    def eat(self, food):
        self.food = food;
        print(f"Eaten {self.food}");
    # @abstractmethod
    def make_sound(self):
        print("Moooo not related sound"); #replace sound

class Cat(Animal):
# def __init__(self, name, breed, age):
#        self.name = name;
#        self.breed = breed;
#        self.age = age;
    def call(self):
       return print(f"Cat sample name {self.name} sample breed {self.breed} sample age {self.age}. ");

    def make_sound(self):
        return print(f"Meow! Meow! \n");
    
class Dog(Animal):
    def call(self):
       return print(f"Dog sample name {self.name} sample breed {self.breed} sample age {self.age}. ");
    def make_sound(self):
        return print(f"Bark bark \n");

dog1 = Dog("Isis", "Dalmation", 15);
dog1.eat("Steak");
dog1.make_sound();
dog1.call();

cat1 = Cat("Puss", "Persian", 4);
cat1.eat("Tuna");
cat1.make_sound();
cat1.call();